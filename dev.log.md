* 4h - before [Sep 23 2019]
  + Preparation of solution.
  + Implementation of backend with SignalR.
  + Implementation of UI with hello world ping back

* 1h 30m - [Sep 23 2019]
  + Refactoring of solution structure due to build error of nested project
  + Implementation of Matrix lib and basic testing code.
