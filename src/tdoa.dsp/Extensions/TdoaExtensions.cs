using System.Collections.Generic;
using tdoa.dsp.Helpers;
using tdoa.dsp.Models;

namespace tdoa.dsp.Extensions
{
    public static class TdoaExtensions
    {
        public static Tdoa ToIdealTdoa(this Tdoa measured, float [][] beacons)
        {
            var idealTdoa = new Tdoa
            {
                Time = measured.Time,
                Position = measured.Position,
                Speed = measured.Speed,
                Td = TdoaCalculator.CalculateIdealTdoa(measured.Position, beacons)
            };

            return idealTdoa;
        }
    }
}