using Newtonsoft.Json;

namespace tdoa.dsp.Models
{
    public class TdoaSimDTO
    {
        [JsonProperty("label")]
        public float Label { get; set; }

        [JsonProperty("tdoaDiff")]
        public TdoaDiff TdoaDiff{ get; set; }
    }
}