using Newtonsoft.Json;

namespace tdoa.dsp.Models
{
    public class TdoaDiff
    {
        [JsonProperty("measured")]
        public Tdoa Measured { get; set; }

        [JsonProperty("calculated")]
        public Tdoa Calculated { get; set; }
    }
}