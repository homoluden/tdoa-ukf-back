using System.Collections.Generic;
using Newtonsoft.Json;
using tdoa.dsp.matrix;

namespace tdoa.dsp.Models
{
    public class Tdoa
    {
        [JsonProperty("time")]
        public float Time { get; set; }

        [JsonProperty("td")]
        public Dictionary<string, float> Td { get; set; }

        [JsonProperty("pos")]
        public float[] Position { get; set; }

        [JsonProperty("speed")]
        public float[] Speed { get; set; }
    }
}