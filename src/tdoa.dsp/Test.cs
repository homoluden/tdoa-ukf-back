using System;
using tdoa.dsp.Helpers;
using tdoa.dsp.matrix;

namespace tdoa.dsp
{
    public static class Test
    {
        public static void MatrixInverseTest()
        {
            var mtx = new Matrix(3, 3, new[] {75f, 12, 12, 21, 100, 21, 31, 100, 31});
            Console.WriteLine($"Test Matrix:");
            mtx.Print();
            
            Console.WriteLine($"Inverse Matrix:");
            mtx.InvPartial().Print();
            
        }

        public static void GlmsTest()
        {
            var H = JacobianBuilder.BuildMatrix();
            
            var tdoaRecords = TDoAParser.ParseFile(@".\data\175934.csv");

            var tdoa0 = tdoaRecords[110];
            var tdoa1 = tdoaRecords[111];
            
            // TDoA difference values converted to Range Differences
            var R0 = tdoa0.ToRDMatrix();
            var R1 = tdoa1.ToRDMatrix();
            
            var gmnk = new GLMS(H, 20, 1e-7f);
            
            var initPos = new Matrix(3, 1, new []
            {
                tdoa0.Position[0], tdoa0.Position[1], tdoa0.Position[2]
            });
            
            var testPos = new Matrix(3, 1, new []
            {
                tdoa1.Position[0], tdoa1.Position[1], tdoa1.Position[2]
            });
            
            var positions = gmnk.ProcessMeasurement(R0, R1, initPos);
            
//            positions.Add(testPos);

            Console.WriteLine("--== Gauss LMS Test ==--");
            Console.Write("Initial Position: ");
            initPos.Transpose().Print();
            Console.WriteLine();
            
            Console.WriteLine("GLMS position updates: ");
            foreach (var position in positions)
            {
                position.Transpose().Print();
            }
            
            Console.WriteLine();
            Console.WriteLine("Desired (test) position: ");
            testPos.Transpose().Print();
            
            Console.WriteLine();
            Console.WriteLine("--== END of Gauss LMS Test ==--");
        }
    }
}