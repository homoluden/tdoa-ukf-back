using System;
using System.Linq;

namespace tdoa.dsp.matrix
{
    public class Matrix
    {
        private float[] _b;
        public readonly int Rows, Cols;
 
        public Matrix(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
            _b = new float[rows * cols];            
        }
 
        public Matrix(int size)
        {
            Rows = size;
            Cols = size;
            _b = new float[Rows * Cols];
            for (int i = 0; i < size; i++)
                this[i, i] = 1;
        }
 
        public Matrix(int rows, int cols, float[] initArray)
        {
            Rows = rows;
            Cols = cols;
            _b = (float[])initArray.Clone();
            if (_b.Length != rows * cols) throw new Exception("bad init array");
        }
 
        public float this[int row, int col]
        {
            get => _b[row * Cols + col];
            set => _b[row * Cols + col] = value;
        }        
 
        public static Vector operator*(Matrix lhs, Vector rhs)
        {
            if (lhs.Cols != rhs.Rows) throw new Exception("I can't multiply matrix by vector");
            var v = new Vector(lhs.Rows);
            for (int i = 0; i < lhs.Rows; i++)
            {
                var sum = 0.0f;
                for (int j = 0; j < rhs.Rows; j++)
                    sum += lhs[i,j]*rhs[j];
                v[i] = sum;
            }
            return v;
        }
 
        public void SwapRows(int r1, int r2)
        {
            if (r1 == r2) return;
            int firstR1 = r1 * Cols;
            int firstR2 = r2 * Cols;
            for (int i = 0; i < Cols; i++)
            {
                var tmp = _b[firstR1 + i];
                _b[firstR1 + i] = _b[firstR2 + i];
                _b[firstR2 + i] = tmp;
            }
        }
 
        //with partial pivot
        public Matrix InvPartial()
        {
            const float eps = 1e-12f;
            if (Rows != Cols) throw new Exception("[Matrix] rows != Cols for Inv!");
            
            var m = new Matrix(Rows); //unitary
            for (int diag = 0; diag < Rows; diag++)
            {
                int maxRow = diag;
                var maxVal = MathF.Abs(this[diag, diag]);
                float d;
                for (int row = diag + 1; row < Rows; row++)
                    if ((d = MathF.Abs(this[row, diag])) > maxVal)
                    {
                        maxRow = row;
                        maxVal = d;
                    }
                
                if (maxVal <= eps) throw new Exception("[Matrix] matrix has too small values!");
                
                SwapRows(diag, maxRow);
                m.SwapRows(diag, maxRow);
                var invd = 1 / this[diag, diag];
                for (int col = diag; col < Cols; col++)
                {
                    this[diag, col] *= invd;
                }
                for (int col = 0; col < Cols; col++)
                {
                    m[diag, col] *= invd;
                }
                for (int row = 0; row < Rows; row++)
                {
                    d = this[row, diag];
                    
                    if (row == diag) continue;
                    
                    for (int col = diag; col < Cols; col++)
                    {
                        this[row, col] -= d * this[diag, col];
                    }
                    
                    for (int col = 0; col < Cols; col++)
                    {
                        m[row, col] -= d * m[diag, col];
                    }
                }
            }

            return m;
        }

        public Matrix Transpose()
        {
            var result = new Matrix(Cols, Rows);
            for (int i = 0; i < Rows; i++)  {
                for (int j = 0; j < Cols; j++)  {
                    result[j, i] = this[i, j];
                }
            }

            return result;
        }

        public override string ToString()
        {
            return $"{Rows} x {Cols} [ {string.Join("  ", _b.Take(3).Select(n => n.ToString()))} {(_b.Length > 3 ? "..." : null)}]";
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                    Console.Write(this[i,j].ToString()+"  ");
                Console.WriteLine();
            }
        }
        
        public static Matrix operator*(Matrix lhs, Matrix rhs)
        {
            var lhsCols = lhs.Cols;
            var rhsRows = rhs.Rows;
            var result = new Matrix(lhs.Rows, rhs.Cols);
            if(lhsCols != rhsRows) {
                throw new InvalidOperationException("[Matrix] Matrix multiplication not possible!");
            }

            for (int i = 0; i < lhs.Rows; i++) {
                for (int j = 0; j < rhs.Cols; j++) {
                    result[i, j] = 0;
                    for (int k = 0; k < lhsCols; k++) {
                        result[i, j] += lhs[i, k] * rhs[k, j];
                    }
                }
            }

            return result;
        }
        
        public static Matrix operator*(Matrix lhs, float scalar)
        {
            var lhsCols = lhs.Cols;
            var lhsRows = lhs.Rows;
            var result = new Matrix(lhs.Rows, lhs.Cols);
            
            for (int i = 0; i < lhs.Rows; i++) {
                for (int j = 0; j < lhs.Cols; j++) {
                    result[i, j] = lhs[i,j] * scalar;
                }
            }

            return result;
        }
        
        public static Matrix operator*(float scalar, Matrix rhs)
        {
            return rhs * scalar;
        }
        
        public static Matrix operator+(Matrix lhs, Matrix rhs)
        {
            var lhsCols = lhs.Cols;
            var lhsRows = lhs.Rows;
            var rhsCols = rhs.Cols;
            var rhsRows = rhs.Rows;
            var result = new Matrix(lhs.Rows, lhs.Cols);
            
            if(lhsCols != rhsCols || lhsRows != rhsRows) {
                throw new InvalidOperationException("[Matrix] Matrix sum not possible!");
            }
            
            for (int i = 0; i < lhs.Rows; i++) {
                for (int j = 0; j < rhs.Cols; j++) {
                    result[i, j] = lhs[i,j] + rhs[i,j];
                }
            }

            return result;
        }
        
        public static Matrix operator-(Matrix lhs, Matrix rhs)
        {
            var lhsCols = lhs.Cols;
            var lhsRows = lhs.Rows;
            var rhsCols = rhs.Cols;
            var rhsRows = rhs.Rows;
            var result = new Matrix(lhs.Rows, lhs.Cols);
            
            if(lhsCols != rhsCols || lhsRows != rhsRows) {
                throw new InvalidOperationException("[Matrix] Matrix sum not possible!");
            }
            
            for (int i = 0; i < lhs.Rows; i++) {
                for (int j = 0; j < rhs.Cols; j++) {
                    result[i, j] = lhs[i,j] - rhs[i,j];
                }
            }

            return result;
        }
    }
}