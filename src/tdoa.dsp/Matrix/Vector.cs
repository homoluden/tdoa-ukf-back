using System;

namespace tdoa.dsp.matrix
{
    public class Vector
    {
        private readonly float[] _b;
        public readonly int Rows;
 
        public Vector(int rows)
        {
            this.Rows = rows;
            _b = new float[rows];
        }
 
        public Vector(float[] initArray)
        {
            _b = (float[])initArray.Clone();
            Rows = _b.Length;
        }

        public float x => this[0];
        public float y => this[1];
        public float z => this[2];

        public Vector Clone()
        {
            Vector v = new Vector(_b);
            return v;
        }
 
        public float this[int row]
        {
            get => _b[row];
            set => _b[row] = value;
        }
 
        public void SwapRows(int r1, int r2)
        {
            if (r1 == r2) return;
            var tmp = _b[r1];
            _b[r1] = _b[r2];
            _b[r2] = tmp;
        }
 
        public float Mag()
        {
            float sum = 0;
            for (int i = 0; i < Rows; i++)
            {
                var d = _b[i];
                sum +=  d*d;
            }
            return MathF.Sqrt(sum);
        }

        public float Norm(float[] weights)
        {
            float sum = 0;
            for (int i = 0; i < Rows; i++)
            {
                var d = _b[i] * weights[i];
                sum +=  d*d;
            }
            return MathF.Sqrt(sum);
        }
 
        public void Print()
        {
            for (int i = 0; i < Rows; i++)
                Console.WriteLine(_b[i]);
            Console.WriteLine();
        }
 
        public static Vector operator-(Vector lhs, Vector rhs)
        {
            var v = new Vector(lhs.Rows);
            for (int i = 0; i < lhs.Rows; i++)
                v[i] = lhs[i] - rhs[i];
            return v;
        }
    }
}