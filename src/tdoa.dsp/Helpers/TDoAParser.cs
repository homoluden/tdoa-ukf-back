using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using tdoa.dsp.matrix;
using tdoa.dsp.Models;

namespace tdoa.dsp.Helpers
{
    public static class TDoAParser
    {
        public static List<Tdoa> ParseFile(string fileRelativePath, string separator = ",")
        {
            
            var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var fullPath = Path.Combine(path, fileRelativePath);
            
            var records = new List<Tdoa>();
            using (var reader = new StreamReader(fullPath))
            {
                _ = reader.ReadLine();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var tdoa = Parse(line, separator);
                    records.Add(tdoa);
                }
            }

            return records;
        }

        private static Tdoa Parse(string line, string separator)
        {
            var strValues = line.Split(separator);
            var result = new Tdoa
            {
                Td = new Dictionary<string, float>()
            };
            
            // Time
            result.Time = ParseFloat(strValues[0]);

            // TDoA for beacon pairs 1-2, 1-3, ..., 1-9, 2-3, ..., 2-9, 3-4, ..., 8-9 (36 pairs in total)
            int k = 1;
            for (int i = 1; i <= 8; i++)
            {
                for (int j = i+1; j <= 9; j++, k++)
                {
                    var td = ParseFloat(strValues[k]);
                    result.Td[$"{i}{j}"] = td;
                }
            }
            
            // Position
            var posN = ParseFloat(strValues[37]);
            var posE = ParseFloat(strValues[38]);
            var posD = ParseFloat(strValues[39]);
            result.Position = new []{posN, posE, posD};
            
            // Speed
            var spdN = ParseFloat(strValues[40]);
            var spdE = ParseFloat(strValues[41]);
            var spdD = ParseFloat(strValues[42]);
            result.Speed = new []{spdN, spdE, spdD};

            return result;
        }

        private static float ParseFloat(string strValue)
        {
            var style = NumberStyles.Float | NumberStyles.AllowDecimalPoint;
            var culture = CultureInfo.CreateSpecificCulture("en-US");
            var success = float.TryParse(strValue, style, culture, out var result);

            return success ? result : float.NaN;
        }
    }
}