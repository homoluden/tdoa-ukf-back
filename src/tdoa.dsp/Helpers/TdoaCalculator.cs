using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using tdoa.dsp.matrix;

namespace tdoa.dsp.Helpers
{
    public static class TdoaCalculator
    {
        /// <summary>
        /// Converts distance in Meters to Time in tech units of 15.25 psec
        /// </summary>
        public static readonly float DistanceToTime = (float)(1.0 / (1e-12 * 15.25 * 299792458));
        
        public static Dictionary<string, float> CalculateIdealTdoa(float[] roverPosition, float[][] beacons)
        {
            var roverPos = new [] {roverPosition[0], roverPosition[1], roverPosition[2]};
            var arrivalDelays = beacons
                .AsParallel()
                .Select(beaconPos => CalculateTd(beaconPos, roverPos))
                .ToArray();

            var result = new Dictionary<string, float>();
            for (int i = 0; i < beacons.Length; i++)
            {
                for (int j = i+1; j < beacons.Length; j++)
                {
                    result[$"{i + 1}{j + 1}"] = arrivalDelays[i] - arrivalDelays[j];
                }
            }

            return result;
        }

        public static Func<float[], float[], float> CalculateTd { get; set; } = (beaconPos, roverPos) =>
        {
//            var dN = 2 * (roverPos[0] - beaconPos[0]); 
//            var dE = 2 * (roverPos[1] - beaconPos[1]); 
//            var dD = 2 * (roverPos[2] - beaconPos[2]);
            var dN = roverPos[0] - beaconPos[0];
            var dE = roverPos[1] - beaconPos[1];
            var dD = roverPos[2] - beaconPos[2];

            var dist = MathF.Sqrt(dN * dN + dE * dE + dD * dD);
            return dist * DistanceToTime;
        };
    }
}