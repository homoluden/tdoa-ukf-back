using tdoa.dsp.Partials;

namespace tdoa.dsp.Helpers
{
    public static class JacobianBuilder
    {
        // TODO: extract beacons to external resource (static helper or external JSON [inputs from frontend or file])
        public static readonly float[][] Beacons = {
            new []{0f,    -3.75f,  3.62f},
            new []{2.60f,  0f,     3.62f},
            new []{6.10f, -1.77f,  2.08f},
            new []{6.10f, -1.24f, -3.82f},
            new []{6.24f,  3.00f, -6.52f},
            new []{3.62f,  0f,    -6.52f},
            new []{0f,    -3.2f,   6.52f},
            new []{0f,     0f,     0f   },
            new []{3.12f,  0f,    -3.6f }
        };

        public static IPartialDerivative[,] BuildMatrix()
        {
            var result = new IPartialDerivative[36, 3];
            
            // Partial Derivatives for beacon pairs 1-2, 1-3, ..., 1-9, 2-3, ..., 2-9, 3-4, ..., 8-9 (36 pairs in total)
            int k = 0;
            for (int i = 0; i < 8; i++)
            {
                for (int j = i+1; j < 9; j++, k++)
                {
                    result[k, 0] = BuildX(i, j);
                    result[k, 1] = BuildY(i, j);
                    result[k, 2] = BuildZ(i, j);
                }
            }

            return result;
        }

        private static IPartialDerivative BuildX(int beaconA, int beaconB)
        {
            var posA = Beacons[beaconA];
            var posB = Beacons[beaconB];
            var pd = new PartialX(posA[0], posA[1], posA[2], posB[0], posB[1], posB[2]);
            return pd;
        }

        private static IPartialDerivative BuildY(int beaconA, int beaconB)
        {
            var posA = Beacons[beaconA];
            var posB = Beacons[beaconB];
            var pd = new PartialY(posA[0], posA[1], posA[2], posB[0], posB[1], posB[2]);
            return pd;
        }

        private static IPartialDerivative BuildZ(int beaconA, int beaconB)
        {
            var posA = Beacons[beaconA];
            var posB = Beacons[beaconB];
            var pd = new PartialZ(posA[0], posA[1], posA[2], posB[0], posB[1], posB[2]);
            return pd;
        }
    }
}