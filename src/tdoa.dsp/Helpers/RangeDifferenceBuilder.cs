using tdoa.dsp.matrix;
using tdoa.dsp.Models;

namespace tdoa.dsp.Helpers
{
    public static class RangeDifferenceBuilder
    {
        // TDoA is measured in 15.25 psec units.
        // Range == Time * SpeedOfLight in atmo
        private const float TdoaToRange = (float)(1e-12 * 15.25 * 299792458); // 0.0045718349845

        public static Matrix ToRDMatrix(this Tdoa record)
        {
            var result = new Matrix(36, 1);
            
            // Range Differences for beacon pairs 1-2, 1-3, ..., 1-9, 2-3, ..., 2-9, 3-4, ..., 8-9 (36 pairs in total)
            int k = 0;
            for (int i = 1; i <= 8; i++)
            {
                for (int j = i+1; j <= 9; j++, k++)
                {
                    result[k, 0] = record.Td[$"{i}{j}"] * TdoaToRange;
                }
            }

            return result;
        } 
    }
}