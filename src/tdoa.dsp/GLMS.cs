using System;
using System.Collections.Generic;
using tdoa.dsp.matrix;
using tdoa.dsp.Partials;

namespace tdoa.dsp
{
    /// <summary>
    /// Gauss Least Mean Squares aka (Hᵀ • H)ˉ¹ • Hᵀ • ΔR = [ ΔX, ΔY, ΔZ]ᵀ
    /// </summary>
    public class GLMS
    {
        private ushort _maxIterations;
        private IPartialDerivative[,] _H;
        private float _breakThreshold;

        /// <summary>
        /// Creates an instance of Gauss LMS Algorithm.
        /// </summary>
        /// <param name="H">Partial derivatives 2D array.</param>
        /// <param name="maxIterations">Maximum number of iterations for Gradient Descent process</param>
        /// <param name="breakThreshold">The distance between two consequent positions at which Gradient Descent process gets terminated.</param>
        public GLMS(IPartialDerivative[,] H, ushort maxIterations = 10, float breakThreshold = 1e-8f)
        {
            _H = H;
            _maxIterations = maxIterations;
            _breakThreshold = breakThreshold;
        }

        /// <summary>
        /// Calculates solution of TDoA problem using Partial Derivative method:
        /// (Hᵀ • H)ˉ¹ • Hᵀ • ΔR = [ X, Y, Z]ᵀ
        /// </summary>
        /// <param name="R0">Range Differences in last known position (calculated as TimeSpeed * TimeDifference)</param>
        /// <param name="R1">New Range Differences in current position (calculated as TimeSpeed * TimeDifference)</param>
        /// <param name="pos0">Last known position of vehicle</param>
        /// <returns>The list of vehicle position updates. Last record is the estimation of current position.</returns>
        public List<Matrix> ProcessMeasurement(Matrix R0, Matrix R1, Matrix pos0)
        {
            // Row count in H and ΔR
            var n = _H.GetLength(0);

            // Column count in H
            var m = _H.GetLength(1);

            var ΔR = new Matrix(n, 1);
            UpdateRangeDifferences(R0, R1, ΔR);
            
            var p = pos0;
            var positionUpdates = new List<Matrix> {p};
        
            var h = new Matrix(n, m);
            UpdatePartialDerivatives(h, p);
            
            for (int k = 0; k < _maxIterations; k++)
            {
                var ht = h.Transpose();
                var pseudoInverse = (ht * h).InvPartial() * ht;

                var Δp = pseudoInverse * ΔR * 0.7f;
                
                p = new Matrix(3, 1, new []{p[0, 0] + Δp[0, 0], p[1, 0] + Δp[1, 0], p[2, 0] + Δp[2, 0]});
                positionUpdates.Add(p);
                
                // Update ΔR
                R0 += h * Δp;
                UpdateRangeDifferences(R0, R1, ΔR);
                
                // Update h
                UpdatePartialDerivatives(h, p);
                
                // Break if Δp is smaller than breakThreshold (steady state)  
                var p0 = positionUpdates[positionUpdates.Count - 2];
                var dist = new Vector(new [] { p[0,0] - p0[0,0], p[1,0] - p0[1,0], p[2,0] - p0[2,0] });
                var distMag = dist.Mag();
                if (k > 1 && distMag <= _breakThreshold)
                {
                    break;
                }
            }

            return positionUpdates;
        }

        private static void UpdateRangeDifferences(Matrix R0, Matrix R1, Matrix ΔR)
        {
            for (int i = 0; i < R0.Rows; i++)
            {
                ΔR[i, 0] = R1[i, 0] - R0[i, 0];
            }
        }

        private void UpdatePartialDerivatives(Matrix h, Matrix p)
        {
            // Iterate over _H elements and do calc like h[i, j] = _H[i, j].Calculate(x, y, z)
            // Where x, y, z are the updated vehicle position
            for (int i = 0; i < h.Rows; i++)
            {
                for (int j = 0; j < h.Cols; j++)
                {
                    h[i, j] = _H[i, j].Calculate(p[0, 0], p[1, 0], p[2, 0]);
                }
            }
        }
    }
}