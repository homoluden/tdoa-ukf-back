using tdoa.dsp.matrix;

namespace tdoa.dsp.Partials
{
    public abstract class PartialBase : IPartialDerivative
    {
        private const float W0 = 0.5f, W1 = 0.25f, W2 = 0.15f, W3 = 0.07f, W4 = 0.3f;
        
        protected readonly float X1;
        protected readonly float Y1;
        protected readonly float Z1;
        protected readonly float X2;
        protected readonly float Y2;
        protected readonly float Z2;

        private float _v0;
        private float _v1;
        private float _v2;
        private float _v3;
        private float _v4;

        protected PartialBase(float x1, float y1, float z1, float x2, float y2, float z2)
        {
            X1 = x1;
            Y1 = y1;
            Z1 = z1;
            X2 = x2;
            Y2 = y2;
            Z2 = z2;
        }

        public float CalculateSmooth(float x, float y, float z)
        {
            var newVal = Calculate(x, y, z);
            _v4 = _v3;
            _v3 = _v2;
            _v2 = _v1;
            _v1 = _v0;
            _v0 = newVal;

            return _v0 * W0 + _v1 * W1 + _v2 * W2 + _v3 * W3 + _v4 * W4;
        }

        public abstract float Calculate(float x, float y, float z);
    }
}