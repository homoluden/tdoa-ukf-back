using System;

namespace tdoa.dsp.Partials
{
    public class PartialY : PartialBase
    {
        public PartialY(float x1, float y1, float z1, float x2, float y2, float z2) : base(x1, y1, z1, x2, y2, z2) {}
        
        public override float Calculate(float x, float y, float z)
        {
            return (y - Y2) / MathF.Sqrt((z - Z2) * (z - Z2) + (y - Y2) * (y - Y2) + (x - X2) * (x - X2))
                - (y - Y1) / MathF.Sqrt((z - Z1) * (z - Z1) + (y - Y1) * (y - Y1) + (x - X1) * (x - X1));
        }
    }
}