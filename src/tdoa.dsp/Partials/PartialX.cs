using System;

namespace tdoa.dsp.Partials
{
    public class PartialX : PartialBase
    {
        public PartialX(float x1, float y1, float z1, float x2, float y2, float z2) : base(x1, y1, z1, x2, y2, z2) {}
        
        public override float Calculate(float x, float y, float z)
        {
            return (2 * (x - Z2) + 2 * (x - X2)) / (2 * MathF.Sqrt((x - Z2) * (x - Z2) + (y - Y2) *(y - Y2) + (x - X2) * (x - X2)))
                - (2 * (x - Z1) + 2 * (x - X1)) / (2 * MathF.Sqrt((x - Z1) * (x - Z1) + (y - Y1) * (y - Y1) + (x - X1) * (x - X1)));
        }
    }
}