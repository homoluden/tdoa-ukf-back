using tdoa.dsp.matrix;

namespace tdoa.dsp.Partials
{
    public interface IPartialDerivative
    {
        float CalculateSmooth(float x, float y, float z);
        
        float Calculate(float x, float y, float z);
    }
}