namespace tdoa.back.Job
{
    public enum TdoaCommand
    {
        // Emty Queue
        NoOp = 0,
        
        // Start Forward Simulation of TDoA problem
        StartTdSim,
        
        // Start solving reverse problem (TDoAs ~> Position Estimations)
        StartTdoaEvaluation
    }
}