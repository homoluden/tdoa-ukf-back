using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace tdoa.back.Job
{
    public sealed class TdoaQueue
    {
        private static readonly Lazy<TdoaQueue> Lazy = new Lazy<TdoaQueue> (() => new TdoaQueue());

        public static TdoaQueue Instance => Lazy.Value;

        private SemaphoreSlim _signal = new SemaphoreSlim(0);
        
        private readonly ConcurrentQueue<TdoaCommand> _q = new ConcurrentQueue<TdoaCommand>();
        
        private TdoaQueue()
        {
        }

        public void Enqueue(TdoaCommand command)
        {
            _q.Enqueue(command);
            _signal.Release();
        }
        
        public async Task<TdoaCommand> DequeueAsync(CancellationToken stopToken)
        {
            await _signal.WaitAsync(stopToken);
            var result = _q.TryDequeue(out var cmd);

            return result ? cmd : TdoaCommand.NoOp;
        } 
    }
}