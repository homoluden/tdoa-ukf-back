using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using tdoa.back.Job;
using tdoa.dsp.Models;

namespace tdoa.back.Hubs
{
    public class DataHub : DynamicHub
    {
        public async Task Ping(string msg)
        {
            await Task.Delay(100);
            await Clients.All.addPong(msg);
        }
        
        public async Task RunTdoaSim()
        {
            await Task.Run(() => TdoaQueue.Instance.Enqueue(TdoaCommand.StartTdSim));
        }
    }
}