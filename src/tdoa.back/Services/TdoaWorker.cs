using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using tdoa.back.Hubs;
using tdoa.back.Job;
using tdoa.dsp.Extensions;
using tdoa.dsp.Helpers;
using tdoa.dsp.Models;

namespace tdoa.back.Services
{
    public class TdoaWorker : BackgroundService
    {
        private readonly ILogger<TdoaWorker> _logger;
        private readonly IHubContext<DataHub> _hub;

        public TdoaWorker(ILogger<TdoaWorker> logger, IHubContext<DataHub> hub)
        {
            _logger = logger;
            _hub = hub;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                _logger.LogInformation("\n[TDOA] Worker started at: {Time}\n", DateTime.Now);
                await CheckQueue(stoppingToken);
            }
        }

        private async Task CheckQueue(CancellationToken stopToken)
        {
            while (!stopToken.IsCancellationRequested)
            {
                var cmd = await TdoaQueue.Instance.DequeueAsync(stopToken);

                switch (cmd)
                {
                    case TdoaCommand.NoOp:
                        // Wait 1sec before checking the queue again
                        await Task.Delay(1000);
                        break;
                    case TdoaCommand.StartTdSim:
                        // Start Forward Simulation
                        await StartTdoaSim();
                        break;
                    case TdoaCommand.StartTdoaEvaluation:
                        // TODO: Implement main routine (TDoA Problem Solving using Kalman Filtering of Gauss LMS)
                        break;
                    default:
                        throw new NotSupportedException();
                }
            }
        }
        
        private async Task StartTdoaSim()
        {
            _logger.LogInformation("\n[TDoA] Starting Forward Simulation...\n");
            
            // 1. Parse beacons data JSON from frontend (3D positions)
            var beacons = JacobianBuilder.Beacons; // TODO: get this data via DataHub request

//            var beacons = new [] {
//                new [] {0.0f, 3.62f, 3.75f}, 
//                new [] {2.60f, 3.62f, 0.0f}, 
//                new [] {6.10f, 2.08f, 1.77f}, 
//                new [] {6.10f, -3.82f, 1.24f}, 
//                new [] {6.24f, -6.52f, 3.00f}, 
//                new [] {3.62f, -6.52f, 0.0f},
//                new [] {0.0f, -6.52f, 3.2f},
//                new [] {0.0f, 0.0f, 0.0f},
//                new [] {3.12f,-3.6f, 0.0f}
//            };
            
            // 2. Read CSV for inputs (time, rover position, rover speed, TDoA measurements)
            var tdoaRecords = TDoAParser.ParseFile(@".\data\175934.csv");
            
            // 3. Calculate ideal TDoA using rover positions
            var tdoaDiffs = tdoaRecords
                .AsParallel()
                .Select(tdoa => new TdoaDiff
                {
                    Measured = tdoa,
                    Calculated = tdoa.ToIdealTdoa(beacons)
                })
                .ToArray();
            
            foreach (var tdoaDiff in tdoaDiffs)
            {
                var data = new TdoaSimDTO
                {
                    Label = tdoaDiff.Measured.Time,
                    TdoaDiff = tdoaDiff
                };

                await _hub.Clients.All.SendAsync("addSimData", data);
                await Task.Delay(20);
            }
        }
    }
}